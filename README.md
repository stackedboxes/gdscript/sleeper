# Sleeper

*Part of [StackedBoxes' GDScript Hodgepodge](https://gitlab.com/stackedboxes/gdscript/)*

A safer alternative to `yield(get_tree().create_timer(0.3), "timeout")`.

The `yield`/`get_tree`/`create_timer` idiom is widely used but is not safe if
the Node calling it has some chance of getting freed before the timeout
completes. If this happens, once the timeout completes, Godot will try to resume
the execution of a method of a Node that is now deleted. In other words, you'd
be executing code that is likely to refer to data that is no longer valid. If
this sounds like a bad idea, that's because it is a bad idea!

An `SbxsSleeper` adds itself as the child of another node (passed to its
constructor), so that, if that node gets freed, the `SbxsSleeper` is freed along
and the `timeout` event never happens.

Use it like this:

```gdscript
func wasHit():
    $TheSprite.modulate = Color.red
    yield(SbxsSleeper.new(self, 0.3), "timeout")
    $TheSprite.modulate = Color.white
```
