#
# Sleeper
# StackedBoxes' GDScript Hodgepodge
#
# Copyright (c) 2021-2022 Leandro Motta Barros
#

extends Timer
class_name SbxsSleeper


# Do we really need this? I am not sure, but I suppose `_selfDestroy()` could be
# scheduled for execution twice if the timer times out at the same frame as we
# exit the tree. Anyway, better safe than sorry. Let's keep the check here to
# make sure we self-destroy only once.
var _done := false


func _init(parent: Node, timeInSecs: float) -> void:
	wait_time = timeInSecs
	autostart = true
	parent.add_child(self)
	one_shot = true
	connect("timeout", self, "_selfDestroy")
	connect("tree_exiting", self, "_selfDestroy")


func _selfDestroy() -> void:
	if _done:
		return
	_done = true
	disconnect("timeout", self, "_selfDestroy")
	disconnect("tree_exiting", self, "_selfDestroy")
	call_deferred("_removeSelfFromParent")
	queue_free()


func _removeSelfFromParent() -> void:
	get_parent().remove_child(self)
